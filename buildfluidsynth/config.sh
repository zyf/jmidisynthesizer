# 配置软件版本
ICONV_VER=1.16
FFI_VER=3.3
GETTEXT_VER=0.21
GLIB_MAIN_VER=2.59
GLIB_VER=2.59.0
OBOE_VER=1.4.3
FLUIDSYNTH_VER=2.1.6

# 文件名
ICONV_FILE=libiconv-${ICONV_VER}.tar.gz
FFI_FILE=libffi-${FFI_VER}.tar.gz
GETTEXT_FILE=gettext-${GETTEXT_VER}.tar.gz
GLIB_FILE=glib-${GLIB_VER}.tar.xz
OBOE_FILE=${OBOE_VER}.tar.gz
FLUIDSYNTH_FILE=v${FLUIDSYNTH_VER}.tar.gz

# 解压后的文件名
ICONV_EX_DIR=libiconv-${ICONV_VER}
FFI_EX_DIR=libffi-${FFI_VER}
GETTEXT_EX_DIR=gettext-${GETTEXT_VER}
GLIB_EX_DIR=glib-${GLIB_VER}
OBOE_EX_DIR=oboe-${OBOE_VER}
FLUIDSYNTH_EX_DIR=fluidsynth-${FLUIDSYNTH_VER}

# 配置软件下载路径
ICONV_URL=http://ftp.gnu.org/pub/gnu/libiconv/${ICONV_FILE}
FFI_URL=ftp://sourceware.org/pub/libffi/${FFI_FILE}
GETTEXT_URL=http://ftp.gnu.org/pub/gnu/gettext/${GETTEXT_FILE}
GLIB_URL=http://ftp.gnome.org/pub/gnome/sources/glib/${GLIB_MAIN_VER}/${GLIB_FILE}
OBOE_URL=https://github.com/google/oboe/archive/${OBOE_FILE}
FLUIDSYNTH_URL=https://github.com/FluidSynth/fluidsynth/archive/${FLUIDSYNTH_FILE}
