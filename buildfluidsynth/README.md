# buildfluidsynth

这个文件夹下的脚本用于一键构建可以用于安卓上使用的 fluidsynth 相关的动态库资源。<br/>
相关的原始资料来源于 fluidsynth 官方的构建安卓的说明页面: [这里](https://github.com/FluidSynth/fluidsynth/wiki/BuildingForAndroid)<br/>
因其使用的脚本已经不适合当前较新版本 NDK，我做了一定的修改适配。

## 构建前的准备工作

### 安装依赖工具
> 当前我只实现了 mac 上的构建。linux 上的没有测试，部分构建指令和配置将不适配<br/>
fluidsynth 构建需要依赖 glib，所以需要安装 autotools
```bash
brew install libtool autoconf automake pkg-config
```

### 下载依赖代码
> 如果你下载的时候出现过慢的情况，可能需要搭个梯子
```bash
bash ./download.sh
```
这个脚本会自动下载所需要的源代码，有更新检测机制。

## 构建

1. 通过环境变量指定 NDK 的根路径和要使用的 ANDROID API 版本。例如我的:
```bash
export NDK_ROOT=/Users/zhangyufei/Library/Android/sdk/ndk/21.3.6528147
export ANDROID_API=21
```
> 注意离开当前终端窗口的时候，这些环境变量将无效
> ANDROID_API 版本指定的时候，28 是一个分界线。28 开始安卓引入了很多新的 posix 接口实现。
> 如果定义了 ANDROID_API >= 28 那么编译出来的动态库将不兼容 28 以下的很多设备

2. 构建指定 ABI 的动态库
```bash
bash ./build.sh arm64-v8a
```
输出结果会在打印的最后一行中有说明，拷贝目录中的动态库到你的项目即可使用<br/>
你可能需要多个 ABI 类型的动态库，重复执行步骤 2 替换 ABI 参数即可
