#!/usr/bin/env bash

# 引入配置项目
. config.sh

curdir=$(cd `dirname $0`;pwd)

function check_need_download() {
    # $1 下载地址
    # $2 文件名

    # 1. 检测本地是否有这个文件
    local_file=$curdir/$2
    if [[ ! -f $local_file ]]; then
       return 1
    fi

    # 2. 进行文件大小的对比
    netsize=$(curl -sfL --head $1 | grep 'Content-Length' | tail -n 1 | awk '{gsub(/\r/, "");print $2}')
    localsize=$(ls -l $local_file | awk '{print $5}')
    if [[ $netsize == $localsize ]]; then
        return 0
    else
        return 1
    fi
}

function download() {
    # $1 下载地址
    # $2 文件名
    echo "检查 $2 是否需要下载"
    check_need_download $1 $2
    if [[ $? == 0 ]]; then
        echo -e "$2 \033[32m已经下载，无需更新\033[0m"
        return 0
    fi

    echo -e "\033[33m开始下载\033[0m $2"
    pushd $curdir
    curl -fLO $1
    popd
}

download $ICONV_URL $ICONV_FILE
download $FFI_URL $FFI_FILE
download $GETTEXT_URL $GETTEXT_FILE
download $GLIB_URL $GLIB_FILE
download $OBOE_URL $OBOE_FILE
download $FLUIDSYNTH_URL $FLUIDSYNTH_FILE
