# jmidisynthesizer

从字面意思上就可以理解这是一个 midi 合成器模块。<br/>
它给安卓上提供了 midi 渲染软音源的能力。<br/>
该模块底层使用的是 [fluidsynth](https://github.com/FluidSynth/fluidsynth)

## 关于 fluidsynth

为了方便，本项目中放置了已经构建好的 fluidsynth 相关的动态库。<br/>
因为在安卓上构建 fluidsynth 非常麻烦，这里有一个单独的说明文档<br/>
以及我整理好的构建脚本可以用于一键构建 fluidsynth 的动态库: [buildfluidsynth/README.md](buildfluidsynth/README.md)<br/>
由于软件协议的限制，所以必须将 fluidsynth 构建成动态库分发

## 运行测试程序

项目包含一个测试应用。按下按钮的时候会发声，抬起来的时候会停止发声。

## 发布

建议使用 aar 进行发布，然后给其它项目使用。在项目根目录执行:
```bash
./gradlew lib:assembleRelease
```

成功后查找一下 aar 文件的路径:
```bash
find ./ -name '*.aar'
```

将找到的 release 版本的 aar 拷走便可使用。
